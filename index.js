require("custom-env").env('production');

const express = require("express");
const app = express();
const { MongoClient, ServerApiVersion } = require("mongodb");
const cors = require("cors");
const http = require("http");
const { PORT, URL } = process.env;

const client = new MongoClient(URL, {
  serverApi: {
    version: ServerApiVersion.v1,
    strict: true,
    deprecationErrors: true,
  },
});

app.use(cors());
app.use(express.urlencoded({ extended: false }));
app.use(express.json());


app.get("/api", async (req, res) => {
  try{
    const database = client.db("atlastore");
    const collection = database.collection("items");
    const result = await collection.find().toArray();
    console.log(result);
    res.setHeader('Cache-Control', 'no-store');
    res.status(200)
    res.send(result);
  } catch(err){
    console.log(err);
    res.status(401)
    res.send(err)
  }
  
});

const server = http.createServer(app);

server.listen(PORT, () => {
  console.log(`Server listening at http://localhost:${PORT}`);
});

module.exports = app